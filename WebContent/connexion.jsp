<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Connexion</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
</head>
<body>

	<div class="container">

		<div class="text-center">
			<h1>Login</h1>
		</div>

		<form class="form col-md-12 center-block" action="./connexion" method="post">
			<div class="form-group">
				<input type="text" class="form-control input-lg" name ="login" placeholder="Login">
			</div>
			<div class="form-group">
				<input type="password" class="form-control input-lg" name ="password" placeholder="Login">
			</div>
			
			<div class="form-group">
				<button type="submit" class="btn btn-primary btn-lg btn-block">Sign-In</button>
			</div>
		</form>

	</div>

	<p>
		<%
			String attribut = (String) request.getAttribute("error");
			if (attribut != null){
				out.println("<div class=\"alert alert-danger\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span><span class=\"sr-only\">Error:</span>" + attribut + "</div>");
			}
		%>
	</p>
</body>
</html>