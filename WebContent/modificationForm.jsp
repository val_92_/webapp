<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%
	if (session.getAttribute("user") == null) {
		out.println("Error, you are not logged in");
		return;
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Modification</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
</head>
<body>

<div class="container">

		<div class="text-center">
			<h1>Modify your info</h1>
		</div>

		<form class="form col-md-12 center-block" action="modify" method="post">
			<div class="form-group">
				<input type="text" class="form-control input-lg" name ="login" placeholder="Login">
			</div>
			<div class="form-group">
				<input type="password" class="form-control input-lg" name ="password" placeholder="Password">
			</div>
			<div class="form-group">
				<input type="text" class="form-control input-lg" name ="name" placeholder="Name">
			</div>
			<div class="form-group">
				<input type="text" class="form-control input-lg" name ="surname" placeholder="Surname">
			</div>
			
			<div class="form-group">
				<button type="submit" class="btn btn-primary btn-lg btn-block">Modify</button>
			</div>
		</form>

	</div>
	
</body>
</html>