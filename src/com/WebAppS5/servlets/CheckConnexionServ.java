package com.WebAppS5.servlets;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Contoller.CheckConnexion;
import Model.User;

public class CheckConnexionServ extends javax.servlet.http.HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String username = request.getParameter("login");
		String password = request.getParameter("password");
		User user = CheckConnexion.Check(username, password);

		if (user != null) {
			request.getSession().setAttribute("user", user);
			System.out.println("ok good");

			response.sendRedirect("modificationForm.jsp");
		} else {
			System.out.println("error login");
			// request.setAttribute("error", "Unknown user, please try again");
			// request.getRequestDispatcher("/login.jsp").forward(request,
			// response);
			String message = "erreur ! Login ou mot de passe incorrect";
			request.setAttribute("error", message);
			this.getServletContext().getRequestDispatcher("/connexion.jsp").forward(request, response);
		}
	}
}
