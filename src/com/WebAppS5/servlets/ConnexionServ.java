package com.WebAppS5.servlets;
import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
public class ConnexionServ extends HttpServlet {
	 public void doGet( HttpServletRequest request, HttpServletResponse response )	throws ServletException, IOException {
	        /* Ne fait rien d'autre qu'appeler une JSP */
	        this.getServletContext().getRequestDispatcher( "/connexion.jsp" ).forward( request, response );
	    }
		
	   
}

