package com.WebAppS5.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Model.Modele;
import Model.User;

public class ModifyInfoServ extends HttpServlet{
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String username = request.getParameter("login");
		String password = request.getParameter("password");
		String name = request.getParameter("nom");
		String surname = request.getParameter("prenom");
		HttpSession sess = request.getSession();
		if(sess.getValue("user") != null)
		{
			User u = (User)sess.getValue("user");
			
			if(check(username))
				u.setLogin(username);
			
			if(check(password))
				u.setPassword(password);
			
			if(check(name))
				u.setNom(name);
			
			if(check(surname))
				u.setPrenom(surname);
			
			Modele m = new Modele();
			try {
				m.saveUser(u);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			sess.setAttribute("user", u);
			
		}else
		{
			System.out.println("error session");
		}
		
	}
	
	private boolean check(String str)
	{
		if(str == null)
		{
			return false;
		}
		return !str.isEmpty();
	}
	
}
