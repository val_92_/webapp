package com.WebAppS5.tests;

import static org.junit.Assert.*;

import java.sql.Date;
import java.sql.SQLException;

import org.junit.Test;

import Model.Modele;
import Model.User;

public class AppTests {
	
	@Test
	public void testConnectionDb()
	{
		Modele m = new Modele();
		assertTrue(m != null);
	}
	
	@Test
	public void testCreationUser()
	{
		Modele m = new Modele();
		User u = new User();
		u.setDateDeNaissance(new Date(1));
		u.setLogin("test");
		u.setPassword("pass");
		u.setNom("jean");
		u.setPrenom("pierre");
		try {
			m.createUser(u);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		User t = null;
		try {
			t = m.connect("test", "pass");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertTrue(t != null || t.getLogin().equals("test") || t.getNom().equals("jean") || t.getPrenom().equals("pierre") || t.getPassword().equals("pass"));
		
	}

}
