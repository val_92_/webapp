package Model;

import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Locale;

import com.mysql.jdbc.*;


public class Modele {
	private User currentUser;
	private Connection connection;
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://localhost/jweb";

	static final String username = "root";
	static final String password = "";
	
	public Modele()
	{
		this.connectToDb();
	}
	
	public User connect(String Login, String Password) throws SQLException
	{
		java.sql.Statement statement = connection.createStatement();
		   PreparedStatement preparedStatement = connection.prepareStatement("select * from user where login=? AND password = ?");
		   preparedStatement.setString(1, Login);
		   preparedStatement.setString(2, Password);
		   ResultSet resultSet = preparedStatement.executeQuery();
		      while (resultSet.next()) {
		    	  System.out.println(resultSet.getString("prenom"));
		    	  String nom = resultSet.getString("nom");
		    	  String prenom = resultSet.getString("prenom");
		    	  String login = resultSet.getString("login");
		    	  String password = resultSet.getString("password");
		    	  Date date = resultSet.getDate("dateDeNaissance");
		    	  int id = resultSet.getInt("id");
		    	  
		    	  currentUser = new User();
		    	  currentUser.setDateDeNaissance(date);
		    	  currentUser.setId(id);
		    	  currentUser.setLogin(login);
		    	  currentUser.setNom(nom);
		    	  currentUser.setPrenom(prenom);
		    	  currentUser.setPassword(password);
		    	  
		    	  return currentUser;
		      }
		      
		return null;
	}
	
	
	public Connection connectToDb()
	{
		Connection conn = null;
		   try{
		      Class.forName("com.mysql.jdbc.Driver");

		      System.out.println("Connecting to database...");
		      conn = (Connection) DriverManager.getConnection(DB_URL,username,password);
		   }
		   catch(Exception e)
		   {
			   System.out.println(e);
		   }
		   this.connection = conn;
		   return connection;
		 
	}
	
	public User getUser(int id) throws SQLException, ParseException
	{
		   java.sql.Statement statement = connection.createStatement();
		   PreparedStatement preparedStatement = connection.prepareStatement("select * from user where id=?");
		   preparedStatement.setInt(1, id);
		   ResultSet resultSet = preparedStatement.executeQuery();
		      while (resultSet.next()) {
		    	  System.out.println(resultSet.getString("prenom"));
		    	  String nom = resultSet.getString("nom");
		    	  String prenom = resultSet.getString("prenom");
		    	  String login = resultSet.getString("login");
		    	  String password = resultSet.getString("password");
		    	  Date date = resultSet.getDate("dateDeNaissance");
		    	  
		    	  User u = new User();
		    	  u.setDateDeNaissance(date);
		    	  u.setId(id);
		    	  u.setLogin(login);
		    	  u.setNom(nom);
		    	  u.setPrenom(prenom);
		    	  u.setPassword(password);
		    	  
		    	  return u;
		      }
		      return null;
	}
	
	public void createUser(User u) throws SQLException 
	{
		java.sql.Statement statement = connection.createStatement();
	   PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO USER(nom,prenom,login,password,dateDeNaissance) values(?,?,?,?,?)");
	   preparedStatement.setString(1, u.getNom());
	   preparedStatement.setString(2, u.getPrenom());
	   preparedStatement.setString(3, u.getLogin());
	   preparedStatement.setString(4, u.getPassword());
	   preparedStatement.setDate(5, u.getDateDeNaissance());
	   boolean flag = preparedStatement.execute();
	   System.out.println(flag);
	}
	
	public void saveUser(User u) throws SQLException
	{
		java.sql.Statement statement = connection.createStatement();
	   PreparedStatement preparedStatement = connection.prepareStatement("UPDATE USER SET nom = ?, prenom = ?, login = ?, password = ?, dateDeNaissance = ? WHERE id = ?");
	   preparedStatement.setString(1, u.getNom());
	   preparedStatement.setString(2, u.getPrenom());
	   preparedStatement.setString(3, u.getLogin());
	   preparedStatement.setString(4, u.getPassword());
	   preparedStatement.setDate(5, u.getDateDeNaissance());
	   preparedStatement.setInt(6, u.getId());
	   boolean flag = preparedStatement.execute();
	   System.out.println(flag);
	}
}
